from flask import *
from flask_sqlalchemy import SQLAlchemy
import hashlib
import json
import sys
import datetime
import jwt
import os



app = Flask('BudgetCTF')
#APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
#APP_STATIC = os.path.join(APP_ROOT, 'static')

#json_url = os.path.join(APP_STATIC, "securityInfo.json")
#f = open(json_url)
#secInfo = json.load(f);

#IL EST PAS CAPABLE DE LIRE UN FICHIER LAISSE TOMBER

secInfos = json.loads(u'{"Flag": "FLAG-2903018","Password-Algorithm": "md5","BestUser": {"Name": "Elon Musk","Password-Pattern": "XXXX-000000000"},"Alert": ["URL:5550/operation/<operation_id> SQL is not secured.","Elon musk just invested in NFTS","Some issues with Bitcoin","URL:5550/user is not finished","Anime is a mistake","Tigers are invading the universe"]}')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./budget.db'
app.config['SECRET_KEY'] = "Tsugi ha jotaro, kusama da!"
db = SQLAlchemy(app)

class TokenSess(db.Model):
	id = db.Column(db.String(32), primary_key=True)

class User(db.Model):
	username = db.Column(db.String(40), primary_key=True)
	firstname = db.Column(db.String(20))
	lastname = db.Column(db.String(20))
	password = db.Column(db.String(50))
	admin = db.Column(db.Boolean)
	budget = db.Column(db.Integer)
	children = db.relationship("Operation")

class Operation(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user = db.Column(db.String(40), db.ForeignKey('user.username'), primary_key=True)
	value = db.Column(db.Integer)


def wrap_user_in_dict_partial(user):
	wrapped = dict()
	wrapped["username"] = user.username
	wrapped["firstname"] = user.firstname
	wrapped["lastname"] = user.lastname
	wrapped["budget"] = user.budget
	return wrapped

def wrap_operation_in_dict_partial(operation):
	wrapped = dict()
	wrapped["id"] = operation.id
	wrapped["user"] = operation.user
	wrapped["value"] = operation.value
	return wrapped

def get_user_from_token(request):
	token = None
	if 'x-access-token' in request.headers:
		token = request.headers['x-access-token']
	if not token:
		raise ValueError('Token is missing.')
	try:
		data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
		print(data, flush=True)
		user_id = data['username']
		return User.query.filter_by(username=user_id).first()
	except Exception as e:
		raise ValueError('Token is invalid '+ str(e))

@app.route('/welcome', methods=['get'])
def welcome():
	current_user = None;
	try:
		current_user = get_user_from_token(request)
	except Exception as e:
		print(str(e), flush =True)
	if not current_user:
		return jsonify({'msg': "Welcome to the budget API (I guess...). Don't worry, it's secured." }), 200
	elif current_user.admin:
		return jsonify({'msg':secInfos}), 200
	return jsonify({'msg': "Welcome to the budget API (I guess...). Don't worry, it's secured." }), 200



@app.route('/login', methods=['post'])
def login():
	auth = request.authorization
	if not auth or not auth.username or not auth.password:
		return jsonify({'message' : 'Could not authenticate you.'}), 401

	user = User.query.filter_by(username=auth.username).first()
	if not user:
		return jsonify({'message' : 'Could not authenticate you.'}), 401

	hashed_password = hashlib.md5(auth.password.encode('utf-8'))
	if hashed_password.hexdigest() == user.password:
		token = jwt.encode({'username': user.username, 'exp': datetime.datetime.utcnow() + datetime.timedelta(days=31)}, app.config["SECRET_KEY"])
		return jsonify({'message' : 'You are successfully authenticated.', 'token': token}), 200

	return jsonify({'message' : 'Could not authenticate you.'}), 401

@app.route('/operation', methods=['POST'])
def create_operation():
	try:
		current_user = get_user_from_token(request)
	except Exception as e:
		print(str(e), flush =True)
		return jsonify({'message':"ACCESS_DENIED"}), 401
	if not current_user:
		return jsonify({'message':"ACCESS_DENIED"}), 401
	try:
		data = request.get_json()
		lastId = Operation.query.count()
		db.engine.execute("INSERT INTO Operation (id, user, value) VALUES (\""+str(lastId)+"\", \""+current_user.username+"\", \""+str(data['value'])+"\");")
		
		newBudget = int(current_user.budget)+int(data['value'])
		current_user.budget = newBudget
		db.session.commit()
	except Exception as e:
		rep = dict()
		rep['message'] = str(e)
		return jsonify(rep), 500
	rep = dict()
	rep['message'] = "operation de"+current_user.username+" cree!"
	return jsonify(rep), 201

@app.route('/user', methods=['POST'])
def create_user():
	try:
		data = request.get_json()
		hashed_password = hashlib.md5(data['password'].encode('utf-8'))
		query = "INSERT INTO User (username, firstname, lastname, password, admin, budget) VALUES (\""+data['username']+"\", \""+data['firstname']+"\", \""+data['lastname']+"\", \""+hashed_password.hexdigest()+"\", FALSE, 0);"
		db.engine.execute(query)
	except Exception as e:
		rep = dict()
		rep['message'] = e
		return jsonify(rep), 500
	rep = dict()
	rep['message'] = "utilisateur "+data['username']+" cree!"
	return jsonify(rep), 201


@app.route('/user', methods=['GET'])
def get_all_user():
	#Roger said that he will work on the role permissions for this root. I hope he will do it quickly :p for now, everyone can access to this route.
	users = User.query.all()
	output = []
	for user in users:
		user_data = wrap_user_in_dict_partial(user)
		output.append(user_data)
	return jsonify({'users': output}), 200

@app.route('/operation', methods=['GET'])
def get_all_operations():
	try:
		current_user = get_user_from_token(request)
	except Exception as e:
		print(str(e), flush =True)
		return jsonify({'message':ACCESS_DENIED}), 401

	operations = Operation.query.filter_by(user= current_user.username)
	output = []
	for operation in operations:
		operation_data = wrap_operation_in_dict_partial(operation)
		output.append(operation_data)
	return jsonify({'operations' : output}), 200

@app.route('/operation/<operation_id>', methods=['GET'])
def get_one_operation(operation_id):
	result = db.engine.execute("SELECT * FROM Operation WHERE id = "+operation_id+";")
	row = result.first()
	operation = Operation(id=row[0], user=row[1], value=row[2])
	operation_data = wrap_operation_in_dict_partial(operation)
	return jsonify({'operation:' : operation_data})

if __name__ == "__main__":
	db.create_all()
	#--THIS PART IS FOR THE CTF. IT'S THE TARGET BECAUSE THE DATABASE IS NOT SAVED--
	try:
		targetPassword = hashlib.md5(b'grrrrrrrr')
		db.session.add(User(username="TigrePuissant", firstname="Frank", lastname="Gagné", password=targetPassword.hexdigest(), admin=True, budget= 0))
		db.session.commit()
	except:
		print("User already created", flush=True)
	try:
		targetPassword = hashlib.md5(b'FLAG-192039029')
		db.session.add(User(username="FLAG-202392389", firstname="Elon", lastname="Musk", password=targetPassword.hexdigest(), admin=False, budget= 29027230801))
		db.session.commit()
	except:
		print("User already created", flush=True)
	app.run(debug=True, host='0.0.0.0', port=5550)
