from flask import Flask, request, jsonify, make_response, render_template
import requests
import json

app = Flask('FrontEnd', template_folder='templates')

BACK_END_IP = 'api1'
BACK_END_PORT = '5550'

def build_home_page(messages):
	content_action = str(messages)

	return render_template('index.html', content_action=content_action)

def build_response(response):
	if response.status_code == 200:
		obj = json.loads(response.content.decode('utf-8'))
		resp = make_response(build_home_page(obj))
		return resp
	else:
		msg = response.content.decode('utf-8')
		resp = make_response(build_home_page({'message':msg}))
		return resp


@app.route('/')
def hello():
	token = request.cookies.get('jwt')
	resp = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT+ '/welcome' ,  headers={'x-access-token':token})
	return build_home_page(resp.text)

@app.route('/createuser', methods=['POST'])
def createuser():
	data = {'username': request.form['username'], 'firstname': request.form['firstname'], 'lastname': request.form['lastname'], 'password': request.form['password']}
	resp = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/user', json=data)
	return build_response(resp)


@app.route('/createoperation', methods=['POST'])
def createOperation():
	token = request.cookies.get('jwt')
	data = {'value': request.form['value']}
	resp = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/operation', json=data,  headers={'x-access-token':token})
	return build_response(resp)


@app.route('/viewoperations', methods=['GET'])
def viewOperations():
	token = request.cookies.get('jwt')
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/operation', headers={'x-access-token':token})
	return build_response(response)


@app.route('/viewoperation', methods=['POST'])
def viewOperation():
	operation_id = request.form['operationId']
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/operation/' + operation_id)
	return build_response(response)

@app.route('/login', methods=['POST'])
def login():
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/login', auth=(request.form['username'], request.form['password']))
	resp = build_response(response)
	if response.status_code == 200:
		obj = json.loads(response.content.decode('utf-8'))
		resp.set_cookie('jwt', obj['token'], httponly=True)
	return resp;

@app.route('/logout', methods=['POST'])
def logout():
	token= request.cookies.get('jwt')
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/logout',headers={'x-access-token':token})
	resp = build_response(response)
	return build_home_page({'msg':'You have been logged out.'})

if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0', port=5551)
