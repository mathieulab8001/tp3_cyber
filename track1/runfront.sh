#!/bin/bash

docker stop paa
docker rm paa -v
docker rmi track1front

docker build -t track1front -f ./project/docker/Dockerfile2 .

docker run -p 5551:5551 --name paa track1front
