from flask import *
from flask_sqlalchemy import SQLAlchemy
import os
import subprocess
import json
import jwt

app = Flask('ShopCTF')

db = SQLAlchemy(app)
seed = 1111111111

app.config['SECRET_KEY']="something super secret"

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50), unique=True)
	password = db.Column(db.String(80))
	admin = db.Column(db.Boolean)

class Item(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))
	description = db.Column(db.String(500))

class PasswordResetKey(db.Model):
	randomInt = db.Column(db.Integer, primary_key=True)
	user = db.Column(db.Integer)

def wrap_user_in_dict(user):
	user_data = {}
	user_data['id'] = user.id
	user_data['name'] = user.name
	user_data['password'] = user.password
	user_data['admin'] = user.admin
	return user_data

def wrap_item_in_dict(item):
	item_data = {}
	item_data['id'] = item.id
	item_data['name'] = item.name
	item_data['description'] = item.description
	return item_data

def wrap_password_reset_key_in_dict(passwordResetKey):
	key_data = {}
	key_data['randomInt'] = passwordResetKey.randomInt
	key_data['user'] = passwordResetKey.user

#Java PRNG
def nextRandom():
	global seed
	seed = (seed * 0x5deece66d + 0xb) & ((1 << 48) - 1)
	randomInt = seed >> 16

	#Unsigned to signed
	if randomInt & (1 << 31):
		randomInt -= (1 << 32)

	return randomInt

def get_user_from_token(request):
	token = None
	if 'x-access-token' in request.headers:
		token = request.headers['x-access-token']

	if not token:
		raise ValueError('Token is missing.')

	try:
		data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
		print(data, flush=True)
		user_id = data['user_id']
		return User.query.filter_by(id=user_id).first()
	except:
		raise ValueError('Token is invalid')


@app.route('/')
def welcome():
	rep = dict()
	rep['message'] = "Welcome to the shop API"
	new_item = Item(name='nom', description='description')
	db.session.add(new_item)
	db.session.commit()
	return rep


@app.route('/item', methods=['GET'])
def get_all_items():
	output = []
	items = Item.query.all()
	for item in items:
		item_data = wrap_item_in_dict(item)
		output.append(item_data)
	return jsonify({'items':output})


@app.route('/user', methods=['GET'])
def get_all_users():
	try:
		current_user = get_user_from_token(request)
	except Exception as e:
		print(str(e), flush=True)
		return jsonify({'message':ACCESS_DENIED}), 401

	if current_user.admin:
		print("get all user route", flush=True)
		users = User.query.all()
		output = []
		for user in users:
			user_data = wrap_user_in_dict(user)
			output.append(user_data)
		return jsonify({'users': output})

	return jsonify({'message':ACCESS_DENIED})


@app.route('/user', methods=['POST'])
def create_user():
	data = request.get_json()
	new_user = User(name=data['name'], password=data['password'], admin=False)
	db.session.add(new_user)
	db.session.commit()
	return jsonify({'message': 'new user created'})


@app.route('/login', methods=['post'])
def login():
	print("LOGIN route", flush=True)

	auth = request.authorization
	if not auth or not auth.username or not auth.password:
		print("LOGIN: problem with request.authorization", flush=True)
		return jsonify({'message' : 'Could not authenticate you.'})

	user = User.query.filter_by(name=auth.username).first()
	if not user:
		print("LOGIN: No such user", flush=True)
		return jsonify({'message' : 'Could not authenticate you.'})

	if user.password == auth.password:
		token = jwt.encode({'user_id':user.id}, app.config['SECRET_KEY'])
		if user.admin == True:
			token = jwt.encode({'user_id':user.id, 'flag':'FLAG-123456789'}, app.config['SECRET_KEY'])

		return jsonify({'message' : 'Your are successfully authenticated. Welcome: ' + user.name, 'token':token})

	print("LOGIN: Wrong password", flush=True)
	return jsonify({'message' : 'Could not authenticate you.'})


@app.route('/getRecoverPasswordNumber', methods=['POST'])
def get_password_key():
	print("get recover password number", flush=True)
	try:
		data = request.get_json()
		current_user = User.query.filter_by(name=data['username']).first()
	except Exception as e:
		print(str(e), flush=True)
		return jsonify({'message':ACCESS_DENIED}), 401

	randomNumber = nextRandom()
	new_reset_key = PasswordResetKey(randomInt=randomNumber, user=current_user.id)
	db.session.add(new_reset_key)
	db.session.commit()
	return jsonify({'randomNumber': randomNumber})


@app.route('/password', methods=['POST'])
def recover_password():
        data = request.get_json()
        user_id = PasswordResetKey.query.filter_by(randomInt=data['key']).first().user
        user = User.query.filter_by(id=user_id).first()
        return jsonify({'password': user.password})

@app.route('/logs', methods=['GET'])
def get_logs():
	try:
		current_user = get_user_from_token(request)
	except Exception as e:
		print(str(e), flush=True)
		return jsonify({'message':ACCESS_DENIED}), 401

	if current_user.admin:
		print("get logs", flush=True)
		output = ['log très long et le premier', 'un autre log qui est aussi assez long et qui fait rien', 'un autre log, mais qui contient le flag cette fois: FLAG-789789', 'un dernier log pour que le flag ne soit pas le dernier']
		return jsonify({'logs': output})

	return jsonify({'message':ACCESS_DENIED})

if __name__ == "__main__":
	db.create_all()
	new_user = User(name='admin', password='FLAG-454545', admin=True)
	db.session.add(new_user)
	db.session.commit()
	app.run(debug=True, host='0.0.0.0', port=5553)
