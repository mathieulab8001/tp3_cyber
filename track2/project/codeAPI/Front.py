from flask import Flask, request, jsonify, make_response, render_template
import requests
import json

app = Flask('FrontEndShop')

BACK_END_IP = 'api2'
BACK_END_PORT = '5553'

def build_home_page(messages):
	token = request.cookies.get('jwt')
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT+ '/', headers={'x-access-token':token})
	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))

	else:
		data = {'message':'ERROR'}

	content_basic = str(data['message'])

	return '''<html>
	<header></header>
	</body>
		<li><a href="/">HOME</a></li>
	<br>
	<br>
	<ul>
		<li>''' + content_basic + '''</li>
	</ul>
	<br>
	<br>
	<br>
	<br>
	LOGIN:<br>
	<form action="/login" method="POST">
		<label>Username : </label>
		<input type="text" placeholder="Enter Username" name="username" id="username" required>
		<label>Password : </label>
		<input type="password" placeholder="Enter Password" name="password" id="password" required>
	<button type="submit" id="login">Login</button>
	</form>
	<br>
	<br>

	CREATE USER:<br>
       <form action="/createuser" method="POST">
               <label>username : </label>
               <input type="text" placeholder="provide the new username" name="name" required>
               <label>password : </label>
               <input type="text" placeholder="provide your password" name="password" required>
	        <button type="submit">Submit</button>
	</form>
	<br>
	<br>

	VIEW USERS: <br>
	<ul>
		<li><a href="/viewusers" id="viewusers">View All users</a></li>
	</ul>
	<br>
	<br>

	GET RECOVER PASSWORD KEY: <br>
	<form action="/getRecoverKey" method="POST">
		<label>Username : </label>
		<input type="text" placeholder="provide your username" name="username" id="recoverName" required>
		<button type="submit" id="recover">Get key</button>
	</form>
	<br>
	<br>

	RECOVER PASSWORD:<br>
       <form action="/getPassword" method="POST">
               <label>Recover password key : </label>
               <input type="number" placeholder="provide the key" name="key" required>
               <button type="submit">Submit</button>
        </form>
	<br>
	<br>

	LOGS:<br>
	<ul>
		<li><a href="/viewlogs">View Logs</a></li>
	</ul>
	<br>
	<br>

	VIEW ITEMS:<br>
	<ul>
		<li><a href="/viewitems">View All Items</a></li>
	</ul>
	</body>
	</html>'''


def build_items_page(response):
        data = None
        if response.status_code == 200:
                data = json.loads(response.content.decode('utf-8'))

        else:
                data = {'items':['ERROR']}

        content = data

        return '''<html>
        <header></header>
        </body>
                <li><a href="/">HOME</a></li>
        <br>
        <br>
        ITEMS:
        <br>
        ''' + str(content['items']) + '''
        </body>
	</html>'''

def build_users_page(response):
	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))

	else:
		data = {'users':['ERROR']}

	return '''<html>
	<header></header>
	</body>
		<li><a href="/">HOME</a></li>
	<br>
	<br>
	USERS:
	<br>
	''' + str(data['users']) + '''
	</body>
	</html>'''

def build_recover_password_number(response):
	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))
	else:
		data = {'randomNumber': 'ERROR'}

	return '''<html>
	<header></header>
	</body>
		<li><a href="/">HOME</a></li>
	<br>
	<br>
	Secret number:
	<br>
	''' + str(data['randomNumber']) + '''
	</body>
	</html>'''

def build_password_page(response):
        data = None
        if response.status_code == 200:
                data = json.loads(response.content.decode('utf-8'))

        else:
                data = {'password':['ERROR']}

        content = data

        return '''<html>
        <header></header>
        </body>
                <li><a href="/">HOME</a></li>
        <br>
        <br>
        PASSWORD:
        <br>
        ''' + str(content['password']) + '''
        </body>
        </html>'''

def build_logs_page(response):
        data = None
        if response.status_code == 200:
                data = json.loads(response.content.decode('utf-8'))

        else:
                data = {'logs':['ERROR']}

        content = data

        return '''<html>
        <header></header>
        </body>
                <li><a href="/">HOME</a></li>
        <br>
        <br>
        LOGS:
        <br>
        ''' + str(content['logs']) + '''
        </body>
        </html>'''

@app.route('/')
def hello():
	return build_home_page({})

@app.route('/login', methods=['POST'])
def login():
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/login', auth=(request.form['username'], request.form['password']))
	resp = make_response(build_home_page({}))
	obj = json.loads(response.content.decode('utf-8'))
	resp.set_cookie('jwt', obj['token'])
	print(resp, flush = True)
	return resp


@app.route('/createuser', methods=['POST'])
def createuser():
	data = {'name': request.form['name'], 'password': request.form['password']}
	print(data, flush=True)
	requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/user', json=data)
	return build_home_page({})

@app.route('/viewusers', methods=['GET'])
def viewUsers():
	token = request.cookies.get('jwt')
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/user', headers={'x-access-token':token})
	return build_users_page(response)


@app.route('/viewitems', methods=['GET'])
def viewItems():
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/item')
	return build_items_page(response)

@app.route('/getRecoverKey', methods=['POST'])
def getRandomNumber():
	data = {'username': request.form['username']}
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/getRecoverPasswordNumber', json=data)
	return build_recover_password_number(response)

@app.route('/getPassword', methods=['POST'])
def getPassword():
	data = {'key': request.form['key']}
	print(data, flush=True)
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/password', json=data)
	return build_password_page(response)

@app.route('/viewlogs', methods=['GET'])
def viewLogs():
	token = request.cookies.get('jwt')
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/logs', headers={'x-access-token':token})
	return build_logs_page(response)

if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0', port=5552)

