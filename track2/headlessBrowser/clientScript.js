const puppeteer = require('puppeteer');

(async () => {
  console.log("start")
  const browser = await puppeteer.launch({userDataDir: "./user_data", args: ['--no-sandbox']});
  console.log("loading login page")
  const loginPage = await browser.newPage();
  await loginPage.setDefaultNavigationTimeout(0);
  await loginPage.goto('http://front2:5552/');
  await loginPage.type('#username','admin');
  await loginPage.type('#password','FLAG-454545');
  await loginPage.click('#login');

  while(true) {
    console.log("Loading users page");
    const page = await browser.newPage();
    await page.goto('http://front2:5552/viewusers');
    await page.waitForTimeout(2000)
    console.log("Loading recover password");
    const recoverPage = await browser.newPage();
    await recoverPage.setDefaultNavigationTimeout(0);
    await recoverPage.goto('http://front2:5552/');
    await recoverPage.type('#recoverName','admin');
    await recoverPage.click('#recover');
    await recoverPage.waitForTimeout(10000)
  }
  await browser.close();
  console.log("fin")
})();
